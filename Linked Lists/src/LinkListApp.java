class LinkListApp {
    public static void main(String[] args) {
        LinkList theList = new LinkList();

        theList.insertFirst(22, 2.99);
        theList.insertFirst(44, 4.99);
        theList.insertFirst(66, 6.99);
        theList.insertFirst(88, 8.99);

        theList.displayList();

        Link aLink = theList.deleteFirst();
        System.out.print("Deleted ");
        aLink.displayLink();
        System.out.println("");

        theList.displayList();
        System.out.println("");

        theList.insertFirst(11, 3.00);
        System.out.print("Insert First");

        System.out.println("");

        
        System.out.println("");


        Link aLink2 = theList.deleteLast();
        System.out.println("");
       System.out.print("Deleted ");
       aLink2.displayLink();
       System.out.println("");

       theList.displayList();
    }
}
