class DoublyLinkedApp {
public static void main(String[] args)
{ 
DoublyLinkedList theList = new DoublyLinkedList();

theList.insertFirst(22); 
theList.printFirstLast();
theList.insertFirst(44);
theList.printFirstLast();
theList.insertFirst(66);
theList.insertLast(11);
theList.insertLast(33);
theList.insertLast(55);
theList.displayForward(); 


theList.displayBackward(); 
theList.deleteFirst(); 
theList.deleteLast(); 
theList.deleteKey(11); 
theList.displayForward(); 
theList.insertAfter(33, 77);
theList.insertAfter(33, 88); 
theList.displayForward(); 
theList.deleteFirst();
System.out.println("Delete First");
theList.displayForward();
theList.deleteLast();
System.out.println("Delete Last");
theList.displayForward();

}
}