public class DoublyLinkedList {
    private Link first;
    private Link last;

    public DoublyLinkedList() { 
        first = null;
        last = null;
    }

    public boolean isEmpty() { 
        return first == null;
    }

    public void insertFirst(long dd) {  //เพิ่มตัวแรก                      เช่น  theList.insertFirst(22);       theList.insertFirst(44);     //first ขยับไปข้างหน้า      44 22
        Link newLink = new Link(dd);   //สร้างobjactใหม่                                                                                   //last อยู่ที่เดิม            last--22 
        if (isEmpty())                 //ถ้าว่างป่าว                                                                                                                  first --44
            last = newLink;            //   last = newLink              last -- 20                              last -- 20            
        else
            first.previous = newLink;   // newList <-- old first                                               NewLink <-- 20
        newLink.next = first;            //newLink --> old first        //ขยับตำแหน่ง first  null --> 20         newLink -->20
        first = newLink;                //first = newlist               first -- 20                             first --44
    }

    public void insertLast(long dd) {    
        Link newLink = new Link(dd);
        if (isEmpty())
            first = newLink;
        else {
            last.next = newLink;
            newLink.previous = last;
        }
        last = newLink;
    }

    public Link deleteFirst() {       //ลบตัวแรก
        Link temp = first;            
        if (first.next == null)       //first อยู่ตัวสุดท้าย
            last = null;              //last ==null (ลบ)
        first.next.previous = null;   //
        first = first.next;
        return temp;
    }

    public Link deleteLast()       //ลบตัวท้าย

    {
        Link temp = last;
        if (first.next == null)
            first = null;
        else
            last.previous.next = null;
        last = last.previous;
        return temp;
    }

    public boolean insertAfter(long key, long dd) {
        Link current = first;
        while (current.dData != key) {
            current = current.next;
            if (current == null)
                return false;
        }
        Link newLink = new Link(dd);
        if (current == last) {
            newLink.next = null;
            last = newLink;
        } else {
            newLink.next = current.next;

            current.next.previous = newLink;
        }
        newLink.previous = current;
        current.next = newLink;
        return true;
    }

    public Link deleteKey(long key) {
        Link current = first;

        while (current.dData != key) {
            current = current.next;
            if (current == null)
                return null;
        }
        if (current == first)
            first = current.next;
        else

            current.previous.next = current.next;
        if (current == last)
            last = current.previous;
        else

            current.next.previous = current.previous;
        return current;
    }

    public void displayForward() {
        System.out.print("List (first-->last): ");
        Link current = first;
        while (current != null) {
            current.displayLink();
            current = current.next;
        }
        System.out.println("");
    }

    public void displayBackward() {
        System.out.print("List (last-->first): ");
        Link current = last;
        while (current != null) {
            current.displayLink();
            current = current.previous;
        }

        System.out.println("");
    }
    public void printFirstLast(){
        System.out.print( "first = "+first+"  ");
        System.out.println("last = "+last);
    }
    
    
}
